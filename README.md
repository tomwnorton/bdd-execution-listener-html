# BDD Execution Listener - HTML Recorder
Serializes the BDD-style models created by the BDD Execution Listener into
an HTML file.  See
[the documentation](https://tomwnorton.bitbucket.io/jvm/bdd-execution-listener-html)
for details.