/*
 * bdd-execution-listener-group
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener.html;

import io.bitbucket.tomwnorton.bdd_execution_listener.Scenario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

class ScenarioViewModel {
    private Collection<String> givenSteps = new ArrayList<>();
    private Collection<String> whenSteps = new ArrayList<>();
    private Collection<ThenStepViewModel> thenSteps = new ArrayList<>();

    static ScenarioViewModel of(Scenario scenario) {
        ScenarioViewModel viewModel = new ScenarioViewModel();
        scenario.getGivens().forEach(viewModel.givenSteps::add);
        scenario.getWhens().forEach(viewModel.whenSteps::add);
        for (Scenario.Step step : scenario.getThens()) {
            viewModel.thenSteps.add(createThenStepViewModel(step));
        }
        return viewModel;
    }

    private static ThenStepViewModel createThenStepViewModel(Scenario.Step step) {
        ThenStepViewModel thenStepViewModel = new ThenStepViewModel();
        thenStepViewModel.value = step.getValue();
        thenStepViewModel.thenClasses = step.getResult() == Scenario.Step.Result.SUCCESS ? "then success" : "then failure";
        step.getThrowable().ifPresent(outerThrowable -> {
            Throwable currentThrowable = outerThrowable;
            do {
                thenStepViewModel.exceptionModels.add(createExceptionViewModel(currentThrowable));
                currentThrowable = currentThrowable.getCause();
            } while (currentThrowable != null);
        });
        return thenStepViewModel;
    }

    private static ExceptionViewModel createExceptionViewModel(Throwable currentThrowable) {
        ExceptionViewModel exceptionViewModel = new ExceptionViewModel();
        exceptionViewModel.header = currentThrowable.getClass()
                                                  .getCanonicalName() + ": " + currentThrowable.getMessage();
        Arrays.stream(currentThrowable.getStackTrace())
              .map(StackTraceElement::toString)
              .forEach(exceptionViewModel.stackTraces::add);
        return exceptionViewModel;
    }

    public Collection<String> getGivenSteps() {
        return givenSteps;
    }

    public Collection<String> getWhenSteps() {
        return whenSteps;
    }

    public Collection<ThenStepViewModel> getThenSteps() {
        return thenSteps;
    }

    static class ThenStepViewModel {
        private String value;
        private String thenClasses;
        private List<ExceptionViewModel> exceptionModels = new ArrayList<>();

        public String getValue() {
            return value;
        }

        public String getThenClasses() {
            return thenClasses;
        }

        public List<ExceptionViewModel> getExceptionModels() {
            return exceptionModels;
        }
    }

    static class ExceptionViewModel {
        private String header;
        private List<String> stackTraces = new ArrayList<>();

        public String getHeader() {
            return header;
        }

        public List<String> getStackTraces() {
            return stackTraces;
        }
    }
}
