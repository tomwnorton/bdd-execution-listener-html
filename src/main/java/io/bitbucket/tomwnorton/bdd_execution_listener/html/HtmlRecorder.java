/*
 * bdd-execution-listener-group
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener.html;

import io.bitbucket.tomwnorton.bdd_execution_listener.Scenario;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.dialect.IPostProcessorDialect;
import org.thymeleaf.engine.ITemplateHandler;
import org.thymeleaf.model.*;
import org.thymeleaf.postprocessor.IPostProcessor;
import org.thymeleaf.postprocessor.PostProcessor;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

public class HtmlRecorder implements Scenario.Recorder {
    private static final String BDD_RESULT_KEY = "bdd-execution-listener-html.output-file";

    private List<ScenarioViewModel> viewModels;

    @Override
    public void start() {
        viewModels = new ArrayList<>();
    }

    @Override
    public void record(Scenario scenario) {
        viewModels.add(createViewModel(scenario));
    }

    @Override
    public void finish() {
        try (Writer writer = createWriter(System.getProperty(BDD_RESULT_KEY, "bdd-results.html"))) {
            ITemplateEngine templateEngine = createTemplateEngine();
            Context context = new Context();
            context.setVariable("scenarios", viewModels);
            templateEngine.process("/io/bitbucket/tomwnorton/bdd_execution_listener/html/template.html", context, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected ScenarioViewModel createViewModel(Scenario scenario) {
        return ScenarioViewModel.of(scenario);
    }

    @SuppressWarnings("SameParameterValue")
    protected Writer createWriter(String fileName) throws IOException {
        return new OutputStreamWriter(new FileOutputStream(fileName));
    }

    protected ITemplateEngine createTemplateEngine() {
        TemplateEngine templateEngine = new TemplateEngine();
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.addDialect(new IPostProcessorDialect() {
            @Override
            public int getDialectPostProcessorPrecedence() {
                return 1;
            }

            @Override
            public Set<IPostProcessor> getPostProcessors() {
                return Collections.singleton(new PostProcessor(TemplateMode.HTML, BlankLineDeletingTemplateHandler.class, 1));
            }

            @Override
            public String getName() {
                return "blank-line-deleting dialect";
            }
        });
        return templateEngine;
    }

    public static class BlankLineDeletingTemplateHandler implements ITemplateHandler {
        private ITemplateHandler next;
        private IText lastBlankLineTextNode;

        private <T> void callNext(BiConsumer<ITemplateHandler, T> consumer, T value, boolean prependLastBlankLineTextNode) {
            if (next != null) {
                if (lastBlankLineTextNode != null && prependLastBlankLineTextNode) {
                    next.handleText(lastBlankLineTextNode);
                }
                consumer.accept(next, value);
            }
            this.lastBlankLineTextNode = null;
        }

        @Override
        public void setNext(ITemplateHandler templateHandler) {
            next = templateHandler;
        }

        @Override
        public void setContext(ITemplateContext templateContext) {
            callNext(ITemplateHandler::setContext, templateContext, true);
        }

        @Override
        public void handleTemplateStart(ITemplateStart templateStart) {
            callNext(ITemplateHandler::handleTemplateStart, templateStart, true);
        }

        @Override
        public void handleTemplateEnd(ITemplateEnd templateEnd) {
            callNext(ITemplateHandler::handleTemplateEnd, templateEnd, false);
        }

        @Override
        public void handleXMLDeclaration(IXMLDeclaration xmlDeclaration) {
            callNext(ITemplateHandler::handleXMLDeclaration, xmlDeclaration, true);
        }

        @Override
        public void handleDocType(IDocType docType) {
            callNext(ITemplateHandler::handleDocType, docType, true);
        }

        @Override
        public void handleCDATASection(ICDATASection cdataSection) {
            callNext(ITemplateHandler::handleCDATASection, cdataSection, true);
        }

        @Override
        public void handleComment(IComment comment) {
            callNext(ITemplateHandler::handleComment, comment, true);
        }

        @Override
        public void handleText(IText text) {
            boolean currentLineIsBlank = text.getText().trim().isEmpty();
            if (currentLineIsBlank) {
                lastBlankLineTextNode = text;
                return;
            }
            callNext(ITemplateHandler::handleText, text, true);
        }

        @Override
        public void handleStandaloneElement(IStandaloneElementTag standaloneElementTag) {
            callNext(ITemplateHandler::handleStandaloneElement, standaloneElementTag, true);
        }

        @Override
        public void handleOpenElement(IOpenElementTag openElementTag) {
            callNext(ITemplateHandler::handleOpenElement, openElementTag, true);
        }

        @Override
        public void handleCloseElement(ICloseElementTag closeElementTag) {
            callNext(ITemplateHandler::handleCloseElement, closeElementTag, true);
        }

        @Override
        public void handleProcessingInstruction(IProcessingInstruction processingInstruction) {
            callNext(ITemplateHandler::handleProcessingInstruction, processingInstruction, true);
        }
    }
}
