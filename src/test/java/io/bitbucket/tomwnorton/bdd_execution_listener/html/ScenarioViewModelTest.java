/*
 * bdd-execution-listener-group
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener.html;

import io.bitbucket.tomwnorton.bdd_execution_listener.Scenario;
import io.bitbucket.tomwnorton.bdd_execution_listener.html.ScenarioViewModel.ExceptionViewModel;
import io.bitbucket.tomwnorton.bdd_execution_listener.html.ScenarioViewModel.ThenStepViewModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class ScenarioViewModelTest {
    private Scenario scenario;

    @BeforeEach
    void setUp() {
        scenario = new Scenario();
        scenario.addGiven("second");
        scenario.addGiven("first");

        scenario.addWhen("fifth");
        scenario.addWhen("fourth");
        scenario.addWhen("third");

        SQLException innerException = new SQLException("The inner message");
        innerException.setStackTrace(new StackTraceElement[] {
                new StackTraceElement("com.fakecompany.NetworkUtils", "sendPackets", "NetworkUtils", 9281),
                new StackTraceElement("com.fakecompany.SQLDriver.ThePreparedStatement", "executeQuery", "SQLDriver", 1023),
                new StackTraceElement("com.example.db.MyRepository", "getForId", "TheRepository", 10)
        });
        RuntimeException exception = new RuntimeException("The outer message", innerException);
        exception.setStackTrace(new StackTraceElement[] {
                new StackTraceElement("com.example.business.MyService", "doSomething", "Service", 97),
                new StackTraceElement("com.example.web.MyController", "getSomething", "MyController", 112)
        });

        scenario.addThen("sixth", Scenario.Step.Result.SUCCESS, null);
        scenario.addThen("seventh", Scenario.Step.Result.SUCCESS, null);
        scenario.addThen("eighth", Scenario.Step.Result.FAILURE, exception);
    }

    @Test
    void it_has_the_appropriate_given_steps() {
        ScenarioViewModel viewModel = ScenarioViewModel.of(scenario);
        assertThat(viewModel.getGivenSteps()).containsExactly("first", "second");
    }

    @Test
    void it_has_the_appropriate_when_steps() {
        ScenarioViewModel viewModel = ScenarioViewModel.of(scenario);
        assertThat(viewModel.getWhenSteps()).containsExactly("third", "fourth", "fifth");
    }

    @Test
    void it_has_the_appropriate_then_steps() {
        ScenarioViewModel viewModel = ScenarioViewModel.of(scenario);
        assertThat(viewModel.getThenSteps().stream().map(ThenStepViewModel::getValue))
                .containsExactly("sixth", "seventh", "eighth");
        assertThat(viewModel.getThenSteps().stream().map(ThenStepViewModel::getThenClasses))
                .containsExactly("then success", "then success", "then failure");
        List<ExceptionViewModel> exceptionViewModels = viewModel.getThenSteps()
                .stream()
                .filter(step -> step.getThenClasses().equals("then failure"))
                .flatMap(step -> step.getExceptionModels().stream())
                .collect(Collectors.toList());
        assertThat(exceptionViewModels.stream().map(ExceptionViewModel::getHeader))
                .containsExactly("java.lang.RuntimeException: The outer message",
                                 "java.sql.SQLException: The inner message");
        assertThat(exceptionViewModels.stream().map(exceptionViewModel -> exceptionViewModel.getStackTraces().size()))
                .containsExactly(2, 3);
        assertThat(exceptionViewModels.stream().flatMap(exceptionViewModel -> exceptionViewModel.getStackTraces().stream()))
                .containsExactly("com.example.business.MyService.doSomething(Service:97)",
                                 "com.example.web.MyController.getSomething(MyController:112)",
                                 "com.fakecompany.NetworkUtils.sendPackets(NetworkUtils:9281)",
                                 "com.fakecompany.SQLDriver.ThePreparedStatement.executeQuery(SQLDriver:1023)",
                                 "com.example.db.MyRepository.getForId(TheRepository:10)");
    }
}