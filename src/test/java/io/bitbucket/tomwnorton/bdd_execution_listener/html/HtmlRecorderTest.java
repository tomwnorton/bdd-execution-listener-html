/*
 * bdd-execution-listener-group
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener.html;

import io.bitbucket.tomwnorton.bdd_execution_listener.Scenario;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.IContext;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.*;

class HtmlRecorderTest {
    private static final String BDD_RESULT_KEY = "bdd-execution-listener-html.output-file";

    @Mock
    private ITemplateEngine templateEngine;

    @Mock
    private Writer writer;

    @Captor
    private ArgumentCaptor<IContext> contextCaptor;

    private HtmlRecorder htmlRecorder;

    private Map<Scenario, ScenarioViewModel> viewModelMap;
    private String expectedFileName = "bdd-results.html";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        viewModelMap = new HashMap<>();
        htmlRecorder = new HtmlRecorder() {
            @Override
            protected ScenarioViewModel createViewModel(Scenario scenario) {
                return viewModelMap.getOrDefault(scenario, new ScenarioViewModel());
            }

            @Override
            protected Writer createWriter(String fileName) {
                if (expectedFileName.equals(fileName)) {
                    return writer;
                }
                return mock(Writer.class);
            }

            @Override
            protected ITemplateEngine createTemplateEngine() {
                return templateEngine;
            }
        };
    }

    @AfterEach
    void clearSystemProperties() {
        System.clearProperty(BDD_RESULT_KEY);
    }

    @SuppressWarnings("unchecked")
    @Test
    void it_can_create_a_result_file() {
        Scenario firstScenario = new Scenario();
        Scenario secondScenario = new Scenario();
        ScenarioViewModel firstViewModel = new ScenarioViewModel();
        ScenarioViewModel secondViewModel = new ScenarioViewModel();

        viewModelMap.put(firstScenario, firstViewModel);
        viewModelMap.put(secondScenario, secondViewModel);

        htmlRecorder.start();
        htmlRecorder.record(firstScenario);
        htmlRecorder.record(secondScenario);
        htmlRecorder.finish();

        verify(templateEngine).process(eq("/io/bitbucket/tomwnorton/bdd_execution_listener/html/template.html"),
                                       contextCaptor.capture(),
                                       same(writer));
        assertThat(contextCaptor.getValue().getVariable("scenarios")).isInstanceOf(List.class);
        assertThat((List<ScenarioViewModel>) contextCaptor.getValue().getVariable("scenarios"))
                .containsExactly(firstViewModel, secondViewModel);
    }

    @Test
    void it_closes_the_writer_when_its_done() throws IOException {
        htmlRecorder.finish();

        InOrder order = inOrder(writer, templateEngine);
        order.verify(templateEngine).process(anyString(), any(IContext.class), any(Writer.class));
        order.verify(writer).close();
    }

    @Test
    void it_closes_the_writer_when_there_is_an_error() throws IOException {
        doThrow(new RuntimeException()).when(templateEngine).process(anyString(), any(IContext.class), any(Writer.class));
        try {
            htmlRecorder.finish();
        } catch (RuntimeException ignored) {
        }
        verify(writer).close();
    }

    @Test
    void it_can_write_the_file_to_a_custom_location() {
        expectedFileName = "my-custom-spot.html";
        System.setProperty("bdd-execution-listener-html.output-file", expectedFileName);

        Scenario firstScenario = new Scenario();
        ScenarioViewModel firstViewModel = new ScenarioViewModel();
        viewModelMap.put(firstScenario, firstViewModel);

        htmlRecorder.start();
        htmlRecorder.record(firstScenario);
        htmlRecorder.finish();

        verify(templateEngine).process(eq("/io/bitbucket/tomwnorton/bdd_execution_listener/html/template.html"),
                                       any(),
                                       same(writer));
    }

    @Test
    void integration_test() {
        Scenario firstScenario = new Scenario();
        firstScenario.addGiven("a widget");
        firstScenario.addWhen("the widget is used");
        firstScenario.addThen("something really cool happens", Scenario.Step.Result.SUCCESS, null);

        Exception innerException = new IOException("An input error occurred");
        innerException.setStackTrace(new StackTraceElement[]{
                new StackTraceElement("com.example.FirstClass", "firstMethod", "FirstFile", 123),
                new StackTraceElement("com.example.SecondClass", "secondMethod", "SecondFile", 1029)
        });
        Exception middleException = new IllegalStateException("Some middle error occurred", innerException);
        middleException.setStackTrace(new StackTraceElement[] {
                new StackTraceElement("com.example.ThirdClass", "thirdMethod", "ThirdFile", 12)
        });
        Exception outerException = new RuntimeException("Some error occurred", middleException);
        outerException.setStackTrace(new StackTraceElement[] {
                new StackTraceElement("com.example.FourthClass", "fourthMethod", "FourthFile", 98),
                new StackTraceElement("com.example.FifthClass", "fifthMethod", "FifthFile", 10_992),
                new StackTraceElement("com.example.SixthClass", "sixthMethod", "SixthFile", 25)
        });
        Scenario secondScenario = new Scenario();
        secondScenario.addGiven("another widget");
        secondScenario.addGiven("a widget");
        secondScenario.addGiven("some magic happens");
        secondScenario.addWhen("the other widget is used");
        secondScenario.addWhen("the widget is used");
        secondScenario.addThen("something very cool happens", Scenario.Step.Result.FAILURE, outerException);

        StringWriter stringWriter = new StringWriter();
        HtmlRecorder htmlRecorder = new HtmlRecorder() {
            @Override
            protected Writer createWriter(String fileName) {
                return stringWriter;
            }
        };

        htmlRecorder.start();
        htmlRecorder.record(firstScenario);
        htmlRecorder.record(secondScenario);
        htmlRecorder.finish();
        String expectedOutput = getExpectedHtmlOutput("expected-output.html");
        assertThat(stringWriter.toString()).isEqualTo(expectedOutput);
    }

    @SuppressWarnings("SameParameterValue")
    private static String getExpectedHtmlOutput(String htmlFile) {
        try (InputStream inputStream = getResourceAsCheckedStream(htmlFile)) {
            List<Byte> bytes = new ArrayList<>();
            int nextChar = inputStream.read();
            while (nextChar != -1) {
                bytes.add((byte) nextChar);
                nextChar = inputStream.read();
            }
            byte[] byteArray = new byte[bytes.size()];
            for (int i = 0; i < byteArray.length; i++) {
                byteArray[i] = bytes.get(i);
            }
            return new String(byteArray, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static InputStream getResourceAsCheckedStream(String htmlFile) {
        InputStream inputStream = HtmlRecorderTest.class.getResourceAsStream(htmlFile);
        if (inputStream == null) {
            throw new IllegalArgumentException("Could not read from resource file: " + htmlFile);
        }
        return inputStream;
    }
}